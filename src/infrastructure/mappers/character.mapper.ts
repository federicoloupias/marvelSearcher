import { CharacterMarvel } from '../../domain/entities/CharacterMarvel.js';
import { Character } from '../interfaces/marvelApi.interfaces.js';

// pedir comics by id 
// https://gateway.marvel.com:443/v1/public/characters/1011334/comics?ts=1000&orderBy=focDate&apikey=3435e8634d1f1cafc1770a0dcb0f1a1c&hash=d0184291b8e83abb3a1dbf173a5e3d86

export class CharacterMapper {

    static marvelApiCharacterToEntity(data: Character): CharacterMarvel {

        const image = `${data.thumbnail.path}.${data.thumbnail.extension}`;

        return {
            id: data.id,
            name: data.name,
            thumbnail: image,
        }
    }

}
