import { marvelApi } from "../../config/api/marvelApi";
import { hash, publicKey, timestamp } from "../../config/hash";
import { CharacterMarvel } from "../../domain/entities/CharacterMarvel";
import { RandomCharacterAPI } from "../../infrastructure/interfaces/randomMarvelApi.interface";
import { CharacterMapper } from "../../infrastructure/mappers/character.mapper";



export const getRandomMarvelCharacter = async (): Promise<CharacterMarvel> => {

    try {

        const randomInt = getRandomInt(1500);

        const url = `/characters?limit=1&&offset=${randomInt}&ts=${timestamp}&apikey=${publicKey}&hash=${hash}`;
        const { data } = await marvelApi.get<RandomCharacterAPI>(url);

        const randomCharacter = CharacterMapper.marvelApiCharacterToEntity(data.data.results[0]);

        return randomCharacter;

    } catch (error) {
        console.log(error);
        throw new Error('Error getting RANDOM marvel character');
    }
}


function getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
}