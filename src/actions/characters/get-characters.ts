import { marvelApi } from "../../config/api/marvelApi";
import { hash, publicKey, timestamp } from "../../config/hash";
import { CharacterMarvel } from "../../domain/entities/CharacterMarvel";
import { Character, MarvelAPIresponse } from "../../infrastructure/interfaces/marvelApi.interfaces";
import { CharacterMapper } from '../../infrastructure/mappers/character.mapper';



export const getMarvelCharacters = async (startsWith: string): Promise<CharacterMarvel[]> => {

    try {
        let url = ''
        if (startsWith) {
            url = `/characters?nameStartsWith=${startsWith}&ts=${timestamp}&apikey=${publicKey}&hash=${hash}`;
        } else {
            url = `/characters?ts=${timestamp}&apikey=${publicKey}&hash=${hash}`;
        }
        const { data } = await marvelApi.get<MarvelAPIresponse>(url);

        const charactersMarvel = data.data.results.slice();

        const charactersMarvelMapper = charactersMarvel.map((item) =>
            CharacterMapper.marvelApiCharacterToEntity(item),
        )

        return charactersMarvelMapper;

    } catch (error) {
        console.log(error);
        throw new Error('Error getting marvel characters');
    }
}
