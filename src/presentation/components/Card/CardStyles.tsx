import styled from "styled-components";

export const CardWrapper = styled.div`
  display: flex;
  width: 256px;
  height: 380px;
  margin: 20px;
  position: relative;
  flex-direction: column;
  box-shadow: 5px 5px 15px rgba(0, 0, 0, 0.9);
  `;

export const CardImage = styled.div<{ background: string }>`
  // linear-gradient(to bottom left, black, transparent)
  flex: 1;
  background-image: linear-gradient(transparent, black), url(${({ background }) => background});
  background-size: cover;
  overflow: hidden; /* Evita que el texto sobrepase el contenedor */
`;

export const CardText = styled.h2`
  position: absolute;
  bottom:0;
  margin-left: 22px;
  margin-bottom: 25px;
  font-size: 18px;
  color: white;
`;

export const FavButtonImage = styled.div`
  position: absolute;
  top:0;
  right: 0;
  margin: 20px;
  color: white;
`;

export const CardTextDate = styled.span`
  font-size: 13px;
`;

