import { StarFilled } from "@ant-design/icons";
import {
    CardWrapper,
    CardImage,
    CardTextDate,
    CardText,
    FavButtonImage,
} from "./CardStyles";

export type CardType = {
    text: string;
    imgUrl: string;
    // color: string;
};


export const Card = ({ text, imgUrl }: CardType) => {
    return (
        <>
            <CardWrapper>
                <CardImage background={imgUrl}>
                    <CardText>{text}</CardText>
                    <FavButtonImage>
                        <StarFilled style={{
                            fontSize: '28px',
                            color: 'white',
                        }} />
                    </FavButtonImage>
                </CardImage>
            </CardWrapper>
        </>
    );
};
