import React from 'react'
import styled from "styled-components";
import marverlImg from '../../../assets/marvelImg.png'
import InputSearch from './InputSearch';
import { SearchOutlined, StarOutlined } from '@ant-design/icons';

const NavbarStyled = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 0px 14px 0px;
`;

const MarvelLogo = styled.img`
/* Layout Properties */
  padding: 8px;
    width: 80px;
    height: 57px;
`

const InputSearchContainer = styled.div` 
  display: flex;
  flex: 1;
  align-items: center;
  padding-inline: 20px;
  border-inline: 1px solid #989898;
`;

const SpaceFinalNavbar = styled.div`
/* Layout Properties */
    width: 80px;
    height: 57px;
`

interface Props {
  onSearch: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Navbar = ({ onSearch }: Props) => {
  return (
    <NavbarStyled>


      {/* Imagen de Marvel */}
      <MarvelLogo src={marverlImg} />


      {/* Contenedor del input con sos icons */}
      <InputSearchContainer>
        <SearchOutlined style={{
          fontSize: '26px',
          color: '#989898'
        }} />
        <InputSearch required onChange={onSearch} type="text" label="Buscar" id="buscar" />

        <StarOutlined style={{
          fontSize: '26px',
          color: '#989898'
        }} />
      </InputSearchContainer>

      {/* Espacio final del navbar vacio */}
      <SpaceFinalNavbar />

    </NavbarStyled>
  )
}



export default Navbar
