import React from 'react'
import styled from 'styled-components'

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  id: string;
  label: string;
}

const InputGroup = styled.div`
  flex: 1;
  position: relative;
`;

const InputLabel = styled.label`
  color: #8d8d8d;
  position: absolute;
  top: 27px;
  left: 55px;
  background: #ffffff;
  transition: 300ms;
  transform: translate(-50%, -50%);
`;

const InputField = styled.input`
flex: 1 !important;
  outline: none;
  padding: 16px 22px;
  font-size: 18px;
  border: 0;

  &:focus
  {
    border: 0
  }

  &:valid + ${InputLabel}
  {
    top: -1px;
    padding: 0 3px;
    font-size:14px;
    color: #8d8d8d;
  }

  &:focus + ${InputLabel}
  {
    top: -1px;
    padding: 0 3px;
    font-size:14px;
    transition: 300ms;
  }
`;
const InputSearch: React.FC<InputProps> = ({ id, label, ...rest }) => {

  return (
    <InputGroup>
      <InputField id={id} onChange={rest.onChange} {...rest} />
      <InputLabel htmlFor={id} >{label}</InputLabel>
    </InputGroup>
  )
}

export default InputSearch
