import React from 'react'

const FailedLoadingScreen = () => {
    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh',
            width: '100vw',
        }}>
            <h1>No se encontraron characters</h1>
        </div>
    )
}

export default FailedLoadingScreen
