import React from 'react';
import styled from 'styled-components';

const Spinner = () => (
  <ViewContainer>
    <StyledSpinnerContainer>
      <StyledSpinner viewBox="0 0 50 50">
        <circle
          className="path"
          cx="25"
          cy="25"
          r="2.5"
          fill="none"
          strokeWidth="0.5"
        />
      </StyledSpinner>
    </StyledSpinnerContainer>
  </ViewContainer>
);

const ViewContainer = styled.div`
display: flex;
width: 100vw;
height: 100vh;
`

const StyledSpinnerContainer = styled.svg`
display: flex;
flex: 1;
`


const StyledSpinner = styled.svg`
width: 80px;
height: 120px;
  animation: rotate 2s linear infinite;
  
  & .path {
    stroke: #5652BF;
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }
  
  @keyframes rotate {
    100% {
      transform: rotate(360deg);
    }
  }
  @keyframes dash {
    0% {
      stroke-dasharray: 1, 150;
      stroke-dashoffset: 0;
    }
    50% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -35;
    }
    100% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -124;
    }
  }
`;

export default Spinner;