import React, { Suspense, useEffect, useState } from 'react'
import Navbar from '../../components/Navbar/Navbar'
import styled from 'styled-components';
import img1 from "../../../assets/cardsImg/imgCard.jpg";
import { Card } from '../../components/Card/Card';
import { useQuery, useSuspenseQuery } from '@tanstack/react-query';
import { getMarvelCharacters, getRandomMarvelCharacter } from '../../../actions/characters';
import Spinner from '../../components/ui/Spinner';
import FailedLoadingScreen from '../../components/ui/FailedLoadingScreen';
import { error, log } from 'console';
import { CharacterMarvel } from '../../../domain/entities/CharacterMarvel';
import { ErrorBoundary } from 'react-error-boundary';

const HomeScreenStyle = styled.div`
    display: flex; 
    flex-direction: column;
    width: 100vw;
    height: 100vh;
`;


const CardContainer = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  flex-wrap: wrap;
  padding: 50px
`;

const Separator = styled.span`
  margin-left: 10px;
  margin-right: 10px;
`;

const HomeScreen = () => {


    const [searchTerm, setSearchTerm] = useState('');

    const [isLoading, setIsLoading] = useState(false);
    const [errorFetching, setErrorFetching] = useState(false);

    const [charactersMatched, setCharactersMatched] = useState<CharacterMarvel[]>();
    const [randomCharacter, setRandomCharacter] = useState<CharacterMarvel>();



    const [timer, setTimer] = useState<NodeJS.Timeout>();

    useEffect(() => {
        setIsLoading(true);
        getRandomMarvelCharacter()
            .then((item) => {
                setRandomCharacter(item);
            })
            .catch(() => {
                setErrorFetching(true);
            })
            .finally(() => {
                setIsLoading(false);
            })
    }, [])


    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setSearchTerm(value);

        // Cancela el temporizador si ya está en marcha
        if (timer) {
            clearTimeout(timer);
            console.log('TIMER del del clear-', timer);
        }

        // Establece un temporizador nuevo para ejecutar la búsqueda después de 500ms
        setTimer(setTimeout(async () => {
            if (!value) {
                setCharactersMatched(undefined);
            } else {
                setCharactersMatched(await getMarvelCharacters(searchTerm))
            }
        }, 500))
    };


    return (
        <HomeScreenStyle style={{ backgroundColor: '#1f2229', flex: 1 }}>
            <Navbar onSearch={handleChange} />
            <ErrorBoundary fallback={<FailedLoadingScreen />}>
                <Suspense fallback={<Spinner />}>
                    <CardContainer>
                        {
                            (charactersMatched && charactersMatched.length > 0)
                                ? charactersMatched.map(charactersMatched => (
                                    <Card key={charactersMatched.id} text={charactersMatched.name} imgUrl={charactersMatched.thumbnail} />
                                ))
                                : (randomCharacter) ? (
                                    <Card key={randomCharacter.id} text={randomCharacter.name} imgUrl={randomCharacter.thumbnail} />
                                )
                                    : null
                        }
                    </CardContainer>
                </Suspense>
            </ErrorBoundary>
        </HomeScreenStyle>
    )
}

export default HomeScreen
