import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeScreen from "./presentation/screens/home/HomeScreen";
import MarvelDetailScreen from "./presentation/screens/marvelDetail/MarvelDetailScreen";
import Navbar from "./presentation/components/Navbar/Navbar";
import styled from "styled-components";
import Spinner from "./presentation/components/ui/Spinner";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";


const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeScreen />} />
          <Route path="marvels/:marvelId" element={<MarvelDetailScreen />} />
          {/* <Redirect path="/marvels" /> */}
        </Routes>
      </BrowserRouter>
    </QueryClientProvider>

  )
}

export default App;
